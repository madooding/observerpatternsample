import java.util.*;


public class WeatherData implements Subject {
	
	private List<Observer> observers;
	private float temp = 0;
	private float humidity = 0;
	private float pressure = 0;
	
	public WeatherData() {
		this.observers = new ArrayList();
	}
	
	@Override
	public void registerObserver(Observer o) {
		this.observers.add(o);

	}

	@Override
	public void removeObserver(Observer o) {
		if(this.observers.indexOf(o) >= 0){
			this.observers.remove(o);
		}
	}

	@Override
	public void notifyObservers() {
		for(Observer o : observers){
			o.update(temp, humidity, pressure);
		}
	}
	
	public void setTemp(float temp){
		this.temp = temp;
		notifyObservers();
	}
	

}
