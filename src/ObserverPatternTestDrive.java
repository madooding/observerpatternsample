
public class ObserverPatternTestDrive {

	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		CurrentConditionsDisplay display = new CurrentConditionsDisplay(weatherData);
		weatherData.setTemp(30);
		
		weatherData.setTemp(42);
	}

}
